package Lista8;

import java.util.LinkedList;

public class GraphAL implements Graph{

//    adjListArray
    LinkedList<Integer> alArray[];
    private int SIZE = 10;

    public GraphAL(){
        alArray= new LinkedList[SIZE];
        for(int i = 0; i < SIZE; i++){
//            alArray[i] = new LinkedList<>();
        }
    }


    @Override
    public boolean addVertex(int vertex) {
        alArray[vertex] = new LinkedList<>();
        return true;
    }

    @Override
    public boolean deleteVertex(int vertex) {
        if(alArray[vertex]==null){
            return false;
        }
        if(alArray[vertex].isEmpty()){
            return false;
        }
        alArray[vertex].clear();
//        Musi usuwać krawędzie
        for(int i =0; i<alArray.length; i++){
            alArray[i].remove(vertex);
        }
        return true;
    }

    @Override
    public boolean addEdge(int vertex0, int vertex1) {
        if (vertex0 < 1 || vertex0 > SIZE || vertex1 < 1 || vertex1 > SIZE)
            return false;
        if(alArray[vertex0]==null||alArray[vertex1]==null)
            return false;
        if(alArray[vertex0].lastIndexOf(vertex1)!=-1)
            return false;
        alArray[vertex0].add(vertex1);
        alArray[vertex1].add(vertex0);
        return true;
    }

    @Override
    public boolean deleteEdge(int vertex0, int vertex1) {
        if (vertex0 < 1 || vertex0 > SIZE || vertex1 < 1 || vertex1 > SIZE)
            return false;
        if(alArray[vertex0].lastIndexOf(vertex1)==-1)
            return false;
        alArray[vertex0].remove(vertex1);
        alArray[vertex1].remove(vertex0);
        return true;
    }

    @Override
    public void write() {

    }

    @Override
    public void read() {

    }

    @Override
    public void printGraph() {
        for(int i = 0; i < alArray.length; i++)
        {
            if(alArray[i]==null){
                continue;
            }
            System.out.println("Adjacency list of vertex "+ i);
            System.out.print("head");
            for(Integer pCrawl: alArray[i]){
                System.out.print(" -> "+pCrawl);
            }
            System.out.println("\n");
        }
    }

    @Override
    public void printEdges() {

    }

    @Override
    public int changeValueOfEdge(int vertex0, int vertex1, int value) {
        return 0;
    }
}
